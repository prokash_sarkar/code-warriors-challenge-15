-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2015 at 06:42 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cwc15_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE IF NOT EXISTS `advertisements` (
`p_id` int(20) NOT NULL,
  `fk_uid` int(20) NOT NULL,
  `p_image` varchar(100) DEFAULT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_catid` int(20) NOT NULL,
  `p_subcatid` int(20) NOT NULL,
  `p_description` varchar(1000) NOT NULL,
  `p_price` double NOT NULL,
  `p_make` varchar(100) NOT NULL,
  `p_model` varchar(100) NOT NULL,
  `p_condition` varchar(100) NOT NULL,
  `p_location` varchar(100) NOT NULL,
  `p_datetime` datetime NOT NULL,
  `p_views` int(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advertisements`
--

INSERT INTO `advertisements` (`p_id`, `fk_uid`, `p_image`, `p_name`, `p_catid`, `p_subcatid`, `p_description`, `p_price`, `p_make`, `p_model`, `p_condition`, `p_location`, `p_datetime`, `p_views`) VALUES
(1, 1, 'fsadfas', 'fasdf', 1, 1, 'fdsfasdf', 20, 'fasdfsd', 'fasfa', 'fasdfads', 'fsadf', '2015-01-23 00:00:00', 1),
(2, 1, 'fasdfasdfasd', 'Nice', 1, 2, 'fasdfasdf', 100, 'fasdfsdfdsfasdf', 'fasfafsdfdsf', 'fasdfadsfasdfasdf', 'fsadffasdfsdf', '2015-01-23 00:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`) VALUES
(1, 'Electronics'),
(2, 'Essentials'),
(3, 'Appliances'),
(4, 'Vehicles');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `fk_uid` int(11) NOT NULL,
  `fk_pid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
`subcat_id` int(11) NOT NULL,
  `fk_catid` int(11) NOT NULL,
  `subcat_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`subcat_id`, `fk_catid`, `subcat_name`) VALUES
(1, 1, 'Laptops'),
(2, 1, 'Desktops'),
(3, 2, 'Hardware tools'),
(4, 4, 'Motorcycle'),
(5, 3, 'Washing machine');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`u_id` int(11) NOT NULL,
  `u_username` varchar(50) NOT NULL,
  `u_key` varchar(100) NOT NULL,
  `u_first_name` varchar(50) NOT NULL,
  `u_last_name` varchar(50) NOT NULL,
  `u_email` varchar(50) NOT NULL,
  `u_mobile_no` varchar(50) NOT NULL,
  `u_type` varchar(20) NOT NULL DEFAULT 'Individual',
  `u_password` text NOT NULL,
  `u_location` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `u_username`, `u_key`, `u_first_name`, `u_last_name`, `u_email`, `u_mobile_no`, `u_type`, `u_password`, `u_location`) VALUES
(1, 'fesfsd', 'e567f8659f635d3b0631e0949f5086d5', 'fsdfds', 'fsdf', 'fsdfsf@yahoo.com', '9999999', 'grerg', '$2a$10$e1fe570779ec431d2b7ecOVSP0CjKIz4Nuc0jkMm4TWEtXChTFTPK', 'fsaff'),
(2, 'fdsfasf', '8dcd8a992d5ddfc4fcf66d24f59a2436', 'fsdfsd', 'fadsfd', 'fsdfa@yahoo.com', '9999999', 'grerg', '$2a$10$524e0d147f3f5944bcda8ugT6mr0Wnh7muBGLdp5koKjw8Yhs6KHm', 'fsaff'),
(3, 'joe', 'f07bb5a37a90817b71c2ecf002941eb5', 'Joe', 'Barton', 'joe@mail.com', '4353445', 'Individual', '$2a$10$a3bb8ae42bd2da5d51809OoQlOoThluFZszrYqOXSFNOnNg32W4rS', 'Dhaka'),
(4, 'tom', 'd95946e7d8296a316cc64100b9ca2246', 'Tom', 'Barton', 'tom@mail.com', '4353445', 'Individual', '$2a$10$1d1b94dba4a046f3267b1u9ijG3bP99IARtLHdg5syQ9lbQwUYB6C', 'Dhaka'),
(5, 'Dan', '2e4246fc895a386035081c0b4ff5dbea', 'Dan', 'Gosling', 'dan@mail.com', '4353445', 'Individual', '$2a$10$0cc6e122a2b8f92da5ac7OFZSsQa1jMDwYMlQT8gEBYJFh.h3kicq', 'Dhaka');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
 ADD PRIMARY KEY (`p_id`), ADD KEY `fk_uid` (`fk_uid`), ADD KEY `p_catid` (`p_catid`), ADD KEY `p_subcatid` (`p_subcatid`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
 ADD KEY `fk_uid` (`fk_uid`), ADD KEY `fk_pid` (`fk_pid`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
 ADD PRIMARY KEY (`subcat_id`), ADD KEY `fk_catid` (`fk_catid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
MODIFY `p_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
MODIFY `subcat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `advertisements`
--
ALTER TABLE `advertisements`
ADD CONSTRAINT `advertisements_ibfk_1` FOREIGN KEY (`fk_uid`) REFERENCES `users` (`u_id`),
ADD CONSTRAINT `fk_catid` FOREIGN KEY (`p_catid`) REFERENCES `categories` (`cat_id`),
ADD CONSTRAINT `fk_subcatid` FOREIGN KEY (`p_subcatid`) REFERENCES `subcategories` (`subcat_id`);

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
ADD CONSTRAINT `favorites_ibfk_1` FOREIGN KEY (`fk_uid`) REFERENCES `users` (`u_id`),
ADD CONSTRAINT `favorites_ibfk_2` FOREIGN KEY (`fk_pid`) REFERENCES `advertisements` (`p_id`);

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
ADD CONSTRAINT `subcategories_ibfk_1` FOREIGN KEY (`fk_catid`) REFERENCES `categories` (`cat_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
