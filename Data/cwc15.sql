-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 27, 2015 at 04:29 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cwc15`
--
CREATE DATABASE IF NOT EXISTS `cwc15` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cwc15`;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `fk_uid` int(11) NOT NULL,
  `fk_pid` int(11) NOT NULL,
  KEY `fk_uid` (`fk_uid`),
  KEY `fk_pid` (`fk_pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(50) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`) VALUES
(1, 'Electronics'),
(2, 'Essentials'),
(3, 'Appliances'),
(4, 'Vehicles');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_uid` int(11) NOT NULL,
  `p_image` varchar(50) DEFAULT NULL,
  `p_name` varchar(50) NOT NULL,
  `p_catid` int(11) NOT NULL,
  `p_subcatid` int(11) NOT NULL,
  `p_description` varchar(1000) NOT NULL,
  `p_price` double NOT NULL,
  `p_condition` varchar(20) NOT NULL,
  `p_date` date NOT NULL,
  PRIMARY KEY (`p_id`),
  KEY `fk_uid` (`fk_uid`),
  KEY `p_catid` (`p_catid`),
  KEY `p_subcatid` (`p_subcatid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `fk_uid`, `p_image`, `p_name`, `p_catid`, `p_subcatid`, `p_description`, `p_price`, `p_condition`, `p_date`) VALUES
(7, 1, NULL, 'Asus laptop', 1, 1, 'Brand new laptop with cherger', 30000, 'Brand New', '2015-01-02'),
(9, 1, NULL, 'Bajaj Pulsar', 4, 4, 'Used pulsar in flying condition', 45000, 'Used', '2015-01-05'),
(10, 3, NULL, 'Konka washing machine', 3, 5, 'Brand new washing machine do all laundry for youuuuu.', 12000, 'Brand new', '2015-01-06'),
(11, 3, NULL, 'Screw driver set', 2, 3, 'Very useful tool set. No bargain.', 5000, 'Brand new', '2015-01-23');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `subcat_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_catid` int(11) NOT NULL,
  `subcat_name` varchar(50) NOT NULL,
  PRIMARY KEY (`subcat_id`),
  KEY `fk_catid` (`fk_catid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`subcat_id`, `fk_catid`, `subcat_name`) VALUES
(1, 1, 'Laptops'),
(2, 1, 'Desktops'),
(3, 2, 'Hardware tools'),
(4, 4, 'Motorcycle'),
(5, 3, 'Washing machine');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(50) NOT NULL,
  `u_email` varchar(50) NOT NULL,
  `Seller_type` varchar(20) NOT NULL DEFAULT 'Individual',
  `u_password` varchar(10) NOT NULL,
  `u_location` varchar(50) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `u_name`, `u_email`, `Seller_type`, `u_password`, `u_location`) VALUES
(1, 'Tom', 'tom@mail.com', 'Individual', 'tomnjerry', 'dhanmondi'),
(2, 'bill', 'gates@mic.com', 'Company', 'martha', 'silicon valley'),
(3, 'Toby', 'toby@weird.com', 'Individual', 'fish', 'begunbari'),
(4, 'Dan', 'dan@blackburn.com', 'Company', 'Scott', 'london');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`fk_uid`) REFERENCES `users` (`u_id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`fk_pid`) REFERENCES `products` (`p_id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_catid` FOREIGN KEY (`p_catid`) REFERENCES `categories` (`cat_id`),
  ADD CONSTRAINT `fk_subcatid` FOREIGN KEY (`p_subcatid`) REFERENCES `subcategories` (`subcat_id`),
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`fk_uid`) REFERENCES `users` (`u_id`);

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_ibfk_1` FOREIGN KEY (`fk_catid`) REFERENCES `categories` (`cat_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
