<?php

require_once '../include/DbHandler-extended.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/register', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('u_username', 'u_first_name', 'u_last_name','u_email', 'u_mobile', 'u_type', 'u_password','u_location'));

            $response = array();

            // reading post params
            $username = $app->request->post('u_username');
            $first_name = $app->request->post('u_first_name');
            $last_name = $app->request->post('u_last_name');
            $email = $app->request->post('u_email');
            $mobile = $app->request->post('u_mobile');
            $type = $app->request->post('u_type');
            $password = $app->request->post('u_password');
            $location = $app->request->post('u_location');

            // validating email address
            validateEmail($email);

            $db = new DbHandler();
            $res = $db->createUser($username, $first_name, $last_name,$email,$mobile,$type,$password,$location);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registering";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(201, $response);
        });

/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/login', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('u_email', 'u_password'));

            // reading post params
            $email = $app->request()->post('u_email');
            $password = $app->request()->post('u_password');
            $response = array();

            $db = new DbHandler();
            // check for correct email and password
            if ($db->checkLogin($email, $password)) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    $response["error"] = false;
                    $response['name'] = $user['u_username'];
                    $response['email'] = $user['u_email'];
                    $response['apiKey'] = $user['u_key'];
                    $response['createdAt'] = $user['created_at'];
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        });


/**
 * Creating new task in db
 * method POST
 * params - name
 * url - /tasks/
 */
$app->post('/postadvert', 'authenticate', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('p_image','p_name','p_catid','p_subcatid','p_description','p_price','p_make','p_model','p_condition','p_location','p_datetime','p_views'));
 
            $response = array();
            
            $p_image = $app->request->post('p_image');
            $p_name = $app->request->post('p_name');
            $p_catid = $app->request->post('p_catid');
            $p_subcatid = $app->request->post('p_subcatid');
            $p_description = $app->request->post('p_description');
            $p_price = $app->request->post('p_price');
            $p_make = $app->request->post('p_make');
            $p_model = $app->request->post('p_model');
            $p_condition = $app->request->post('p_condition');
            $p_location = $app->request->post('p_location');
            $p_datetime = $app->request->post('p_datetime');
            $p_views = $app->request->post('p_views');
 
            global $user_id;

           

            $db = new DbHandler();
 
            // creating new task
            $ad_id = $db->createAdvert($user_id, $p_image,$p_name,$p_catid,$p_subcatid,$p_description,$p_price,$p_make,$p_model,$p_condition,$p_location,$p_datetime,$p_views);
 
            if ($ad_id != NULL) {
                $response["error"] = false;
                $response["message"] = "Ad created successfully";
                $response["ad_id"] = $ad_id;
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to create ad. Please try again";
            }
            echoRespnse(201, $response);
        });



        $app->get('/getallads', function() use ($app) {
            
            $response = array();
            $db = new DbHandler();
 
            // fetch task
            $result = $db->getAllAds();
            
            $json=array(); 
            

            if ($result != NULL) {
              


              //$obj = $result->fetch_object();
                
                while($obj =mysqli_fetch_object($result))
                {   
                   /* $outer[
                    $json['error'] = "false";
                    $json['id'] = $obj->p_id;
                    $json['name'] = $obj->p_name;
                    $json['price'] = $obj->p_price;
                    $json['createdAt'] = $obj->p_datetime;
                    $json['condition'] = $obj->p_condition;
                    ]*/
                    $json[]=$obj;
                }
    
                /*$response["error"] = false;
                $response["id"] = $obj->p_id;
                $response["name"] = $obj->p_name;
                $response["price"] = $obj->p_price;
                $response["createdAt"] = $obj->p_datetime;
                $response["condition"]=$obj->p_condition;*/

              
                echo '200 '."\r\n";
                echo json_encode($json);
                //echoRespnse(200, $json);

                
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exist";
                echoRespnse(404, $response);
            }
        });


        $app->get('/getadsbyid', 'authenticate', function() use ($app) {
            global $user_id;
            $response = array();
            $db = new DbHandler();
 
            // fetch task
            $result = $db->getUserAds($user_id);
 
            if ($result != NULL) {
                 while($obj =mysqli_fetch_object($result))
                {  
                    $json[]=$obj;
                }
    
               
                echo '200 '."\r\n";
                echo json_encode($json);
                //echoRespnse(200, $response);

            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });


        //getting ads by category

        $app->get('/getadsbycategory/:cat_name', function ($cat_name) use ($app) {
            
             //verifyRequiredParams(array('cat_name'));

            $response = array();

            //$cat_name = $app->request()->get('cat_name');

            $response = array();

            echo $cat_name;

            $db = new DbHandler();
 
            // fetch task
            $result = $db->getAdsByCategory($cat_name);
 
            if ($result != NULL) {
                 while($obj =mysqli_fetch_object($result))
                {  
                    $json[]=$obj;
                }
    
               
                echo '200 '."\r\n";
                echo json_encode($json);
                //echoRespnse(200, $response);

            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });


        //getting ads by subcategory

        $app->get('/getadsbysubcategory/:subcat_name', function ($subcat_name) use ($app) {
            
             //verifyRequiredParams(array('cat_name'));

            $response = array();

            //$cat_name = $app->request()->get('cat_name');

            $response = array();

            //echo $cat_name;

            $db = new DbHandler();
 
            // fetch task
            $result = $db->getAdsBySubcategory($subcat_name);
 
            if ($result != NULL) {
                 while($obj =mysqli_fetch_object($result))
                {  
                    $json[]=$obj;
                }
    
               
                echo '200 '."\r\n";
                echo json_encode($json);
                //echoRespnse(200, $response);

            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });


/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */




/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>