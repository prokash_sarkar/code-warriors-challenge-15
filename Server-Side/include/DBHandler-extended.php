<?php
 
/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 *
 */
class DbHandler {
 
    private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . './DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
 
    /* ------------- `users` table method ------------------ */
 
    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($u_username,$u_first_name, $u_last_name, $u_email, $u_mobile, $u_type, $u_password, $u_location) {
        require_once 'PassHash.php';
        $response = array();
 
        // First check if user already existed in db
        if (!$this->isUserExists($u_email)) {
            // Generating password hash
            $password_hash = PassHash::hash($u_password);
 
            // Generating API key
            $api_key = $this->generateApiKey();
 
            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(u_username, u_key, u_first_name, u_last_name, u_email, u_mobile_no, u_type, u_password, u_location) values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("sssssssss", $u_username, $api_key, $u_first_name, $u_last_name, $u_email, $u_mobile, $u_type, $password_hash, $u_location);
 
            $result = $stmt->execute();
 
            $stmt->close();
 
            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }
 
        return $response;
    }
 
   /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT u_password FROM users WHERE u_email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }
 
    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT u_id from users WHERE u_email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
 
    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT * FROM users WHERE u_email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
 
    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT u_key FROM users WHERE u_id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $api_key = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }
 
    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT u_id FROM users WHERE u_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }
 
    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT u_id from users WHERE u_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
 
    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }
 
    /* ------------- `tasks` table method ------------------ */
 
    /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
    public function createTask($user_id, $task) {        
        $stmt = $this->conn->prepare("INSERT INTO tasks(task) VALUES(?)");
        $stmt->bind_param("s", $task);
        $result = $stmt->execute();
        $stmt->close();
 
        if ($result) {
            // task row created
            // now assign the task to user
            $new_task_id = $this->conn->insert_id;
            $res = $this->createUserTask($user_id, $new_task_id);
            if ($res) {
                // task created successfully
                return $new_task_id;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            // task failed to create
            return NULL;
        }
    }
 
    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.task, t.status, t.created_at from tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        if ($stmt->execute()) {
            $task = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $task;
        } else {
            return NULL;
        }
    }
 
    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    public function getAllUserTasks($user_id) {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }
 
    /**
     * Updating task
     * @param String $task_id id of the task
     * @param String $task task text
     * @param String $status task status
     */
    public function updateTask($user_id, $task_id, $task, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.task = ?, t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $task, $status, $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }
 
    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }
 
    /* ------------- `user_tasks` table method ------------------ */
 
    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }

    
    //***** extended functions for handling advertisemts ********


    //function to create new advertisement
    public function createAdvert($fk_uid, $p_image, $p_name, $p_catid, $p_subcatid, $p_description, $p_price,$p_make,$p_model,$p_condition, $p_location,$p_datetime,$p_views) {
        $stmt = $this->conn->prepare("INSERT INTO advertisements(fk_uid, p_image, p_name, p_catid, p_subcatid, p_description, p_price,p_make,p_model,p_condition, p_location,p_datetime,p_views) values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("issiisdsssssi", $fk_uid, $p_image, $p_name, $p_catid, $p_subcatid, $p_description, $p_price,$p_make,$p_model,$p_condition, $p_location,$p_datetime,$p_views);
        $result = $stmt->execute();
        $stmt->close();

        if($result)
        {
            //echo 'ad created successfully';
            return TRUE;
        }
        else
        {
            //echo 'failed to create ad';
            return FALSE;
        }
        return null;
    }

    public function getAllAds() {
        $stmt = $this->conn->prepare("SELECT * FROM advertisements");
        //$stmt->bind_param("i", $user_id);
        $stmt->execute();
        $ads = $stmt->get_result();
        $stmt->close();
        return $ads;
    }

     public function getUserAds($user_id) {
        $stmt = $this->conn->prepare("SELECT p.* FROM advertisements p, users u WHERE p.fk_uid=u.u_id AND u.u_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $ads = $stmt->get_result();
        $stmt->close();
        return $ads;
    }


    public function getLatestAds($user_id) {
        $stmt = $this->conn->prepare("SELECT * FROM products ORDER BY p_date DESC");
        $stmt->execute();
        $ads = $stmt->get_result();
        $stmt->close();
        return $ads;
    }

    public function getAdsByCategory($cat_name) {
        $stmt = $this->conn->prepare("SELECT p.* FROM advertisements p, categories c WHERE p.p_catid=c.cat_id AND c.cat_name = ?");
        $stmt->bind_param("s", $cat_name);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    public function getAdsBySubcategory($subcat_name) {
        $stmt = $this->conn->prepare("SELECT p.* FROM advertisements p, categories c, subcategories s WHERE p.p_catid=c.cat_id AND s.fk_catid = c.cat_id  AND s.subcat_name= ?");
        $stmt->bind_param("s",$subcat_name);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

     public function getAdsByPriceRange($min_price,$max_price) {
        $stmt = $this->conn->prepare("SELECT * FROM products WHERE p_price>=? AND p_price<=?");
        $stmt->bind_param("ii", $min_price,$max_price);
        $stmt->execute();
        $ads = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }


    //this query requires a location column to be added to the users table

    public function getAdsByLocation($location) {
        $stmt = $this->conn->prepare("SELECT p.* FROM products p, users u WHERE p.fk_uid=u.u_id AND u.u_location=?;");
        $stmt->bind_param("s", $location);
        $stmt->execute();
        $ads = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

 
}
 
?>