package com.ulab.cwc;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import ru.truba.touchgallery.GalleryWidget.BasePagerAdapter.OnItemChangeListener;
import ru.truba.touchgallery.GalleryWidget.GalleryViewPager;
import ru.truba.touchgallery.GalleryWidget.UrlPagerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AdDetailsActivity extends Activity {

	private GalleryViewPager mViewPager;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ad_details);
		String[] urls = {
				"http://cs407831.userapi.com/v407831207/18f6/jBaVZFDhXRA.jpg",
				"http://cs407831.userapi.com/v4078f31207/18fe/4Tz8av5Hlvo.jpg",
				"http://cs407831.userapi.com/v407831207/1906/oxoP6URjFtA.jpg",
				"http://cs407831.userapi.com/v407831207/190e/2Sz9A774hUc.jpg",
				"http://cs407831.userapi.com/v407831207/1916/Ua52RjnKqjk.jpg",
				"http://cs407831.userapi.com/v407831207/191e/QEQE83Ok0lQ.jpg" };
		List<String> items = new ArrayList<String>();
		Collections.addAll(items, urls);

		UrlPagerAdapter pagerAdapter = new UrlPagerAdapter(this, items);
		pagerAdapter.setOnItemChangeListener(new OnItemChangeListener() {
			@Override
			public void onItemChange(int currentPosition) {
				Toast.makeText(AdDetailsActivity.this,
						"Current item is " + currentPosition,
						Toast.LENGTH_SHORT).show();
			}
		});

		mViewPager = (GalleryViewPager) findViewById(R.id.viewer);
		mViewPager.setOffscreenPageLimit(3);
		mViewPager.setAdapter(pagerAdapter);

	}

}