package com.ulab.cwc;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ulab.cwc.utility.AlertDialogManager;
import com.ulab.cwc.utility.ConnectionDetector;
import com.ulab.cwc.utility.FormValidationManager;
import com.ulab.cwc.utility.SessionManager;

public class LoginActivity extends Activity {

	private static final String LOGIN_URL = "http://192.168.56.1/code-warriors-challenge-15/Server-Side/v1/login";

	// progress dialog
	private ProgressDialog pDialog;

	// Session Manager Class
	private SessionManager session;

	// flag for Internet connection status
	private Boolean isInternetPresent = false;

	// Connection detector class
	private ConnectionDetector cd;

	// Alert Dialog Manager
	private AlertDialogManager alert;

	protected EditText mEmail;
	protected EditText mPassword;
	protected Button mLoginButton;
	protected TextView mSignUpTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		mEmail = (EditText) findViewById(R.id.emailField);
		mPassword = (EditText) findViewById(R.id.passwordField);

		mLoginButton = (Button) findViewById(R.id.loginButton);
		mLoginButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				// Get EditText values
				String email = mEmail.getText().toString();
				String password = mPassword.getText().toString();

				// Instantiate Http Request Param Object
				RequestParams params = new RequestParams();

				// get Internet status
				isInternetPresent = cd.isConnectingToInternet();

				// check for Internet status
				if (isInternetPresent) {
					// Internet Connection is Present

					// validate text fields
					if (!FormValidationManager.validate(email)) {
						mEmail.setError("Invalid Email");
					} else if (!FormValidationManager.isNotNull(password)) {
						mPassword.setError("Invalid Password");
					} else {
						// Put Http parameter email with value of Email Edit
						// View
						// control
						params.put("u_email", email);
						// Put Http parameter password with value of Password
						// Edit View
						// control
						params.put("u_password", password);
						// Invoke RESTful Web Service with Http parameters
						invokeWS(params);
					}
				} else {
					// Internet connection is not present
					// Ask user to connect to Internet
					alert.showAlertDialog(LoginActivity.this,
							"No Internet Connection",
							"You don't have internet connection.", false);
				}
			}
		});

		mSignUpTextView = (TextView) findViewById(R.id.signUpText);
		mSignUpTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this,
						SignUpActivity.class);
				startActivity(intent);
			}
		});

		mLoginButton = (Button) findViewById(R.id.loginButton);

		// Session Manager
		session = new SessionManager(getApplicationContext());

		// creating connection detector class instance
		cd = new ConnectionDetector(getApplicationContext());

		// creating alert dialog class instance
		alert = new AlertDialogManager();

	}

	/**
	 * Method that performs RESTful web service invocations
	 * 
	 * @param params
	 */
	public void invokeWS(RequestParams params) {

		// Instantiate Progress Dialog object
		pDialog = new ProgressDialog(this);
		// Set Progress Dialog Text
		pDialog.setMessage("Please wait...");
		// Set Cancelable as False
		pDialog.setCancelable(false);
		// Show Progress Dialog
		pDialog.show();

		// Make RESTful webservice call using AsyncHttpClient object
		AsyncHttpClient client = new AsyncHttpClient();

		client.post(LOGIN_URL, params, new AsyncHttpResponseHandler() {
			// When the response returned by REST has Http response code '200'
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// TODO Auto-generated method stub
				// Hide Progress Dialog
				pDialog.hide();
				// When Http response code is '404'
				if (statusCode == 404) {
					Toast.makeText(getApplicationContext(),
							"Requested resource not found", Toast.LENGTH_LONG)
							.show();
				}
				// When Http response code is '500'
				else if (statusCode == 500) {
					Toast.makeText(getApplicationContext(),
							"Something went wrong at server end",
							Toast.LENGTH_LONG).show();
				}
				// When Http response code other than 404, 500
				else {
					Toast.makeText(
							getApplicationContext(),
							"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]",
							Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] responseStream) {
				// TODO Auto-generated method stub
				if (pDialog.isShowing()) {
					// Hide Progress Dialog
					pDialog.hide();
					pDialog.dismiss();
				}

				// Parse the response
				try {
					String response = IOUtils.toString(responseStream, "UTF_8");

					// JSON Object
					JSONObject obj = new JSONObject(response);

					// When the JSON response has status boolean value assigned
					// with true
					if ((obj.getBoolean("error")) != true) {

						// Creating user login session
						session.createLoginSession(mEmail.getText().toString(),
								mPassword.getText().toString());

						// Display successfully registered message using Toast
						Toast.makeText(getApplicationContext(),
								"You are successfully logged in!",
								Toast.LENGTH_LONG).show();

						// Redirecting to the main class
						Intent i = new Intent(getApplicationContext(),
								MainActivity.class);
						// Clears History of Activity
						i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(i);
						finish();
					}
					// Else display error message
					else {
						Toast.makeText(getApplicationContext(),
								obj.getString("message"), Toast.LENGTH_LONG)
								.show();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				catch (JSONException e) {
					// TODO Auto-generated catch block
					Toast.makeText(
							getApplicationContext(),
							"Error Occured [Server's JSON response might be invalid]!",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();

				}
			}
		});
	}

}
