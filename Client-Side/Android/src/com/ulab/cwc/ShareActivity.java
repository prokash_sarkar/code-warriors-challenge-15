package com.ulab.cwc;

import com.google.android.gms.plus.PlusShare;
import com.ulab.cwc.share.FacebookShareActivity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ShareActivity extends Activity {

	// Buttons
	Button btnFBShare, btngShare, btnDialer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Facebook share button click event

		btnFBShare.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Clear the session data
				Intent fb = new Intent(getApplicationContext(),
						FacebookShareActivity.class);
				startActivity(fb);
			}
		});

		// Google share button click event

		btngShare.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Launch the Google+ share dialog with attribution to your app.
				Intent shareIntent = new PlusShare.Builder(ShareActivity.this)
						.setType("text/plain")
						.setText("Welcome to the Code Warriors Challenge 2015")
						.setContentUrl(
								Uri.parse("https://developers.google.com/+/"))
						.getIntent();

				startActivityForResult(shareIntent, 0);
			}
		});

		// Number dialer

		btnDialer.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try {
					Intent my_callIntent = new Intent(Intent.ACTION_CALL);
					String phn_no = null;
					// here the word 'tel' is important for making a call...
					startActivity(my_callIntent);
				} catch (ActivityNotFoundException e) {
					Toast.makeText(getApplicationContext(),
							"Error in your phone call" + e.getMessage(),
							Toast.LENGTH_LONG).show();
				}
			}
		});

	}
}