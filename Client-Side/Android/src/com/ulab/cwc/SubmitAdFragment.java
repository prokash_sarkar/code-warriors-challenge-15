package com.ulab.cwc;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ulab.cwc.utility.AlertDialogManager;
import com.ulab.cwc.utility.ConnectionDetector;
import com.ulab.cwc.utility.FormValidationManager;
import com.ulab.cwc.utility.SessionManager;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SubmitAdFragment extends Fragment {

	private static final String AD_SUBMIT_URL = "http://192.168.56.1/code-warriors-challenge-15/Server-Side/v1/postadvert";

	// progress dialog
	private ProgressDialog pDialog;

	// Session Manager Class
	private SessionManager session;

	// flag for Internet connection status
	private Boolean isInternetPresent = false;

	// Connection detector class
	private ConnectionDetector cd;

	// Alert Dialog Manager
	private AlertDialogManager alert;

	protected EditText mProductName;
	protected EditText mProductDescription;
	protected EditText mProductPrice;
	protected EditText mProductBrand;
	protected EditText mProductModel;
	protected EditText mProductCondition;
	protected EditText mProductLocation;
	protected Button mSubmitButton;
	protected Button mCancelButton;

	public SubmitAdFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_submit_ad,
				container, false);
		mProductName = (EditText) rootView.findViewById(R.id.productNameField);
		mProductDescription = (EditText) rootView
				.findViewById(R.id.productDescriptionField);
		mProductPrice = (EditText) rootView
				.findViewById(R.id.productPriceField);
		mProductBrand = (EditText) rootView
				.findViewById(R.id.productBrandField);
		mProductModel = (EditText) rootView
				.findViewById(R.id.productModelField);
		mProductCondition = (EditText) rootView
				.findViewById(R.id.productConditionField);
		mProductLocation = (EditText) rootView
				.findViewById(R.id.productLocationField);

		// Session Manager
		session = new SessionManager(getActivity().getApplicationContext());

		// creating connection detector class instance
		cd = new ConnectionDetector(getActivity().getApplicationContext());

		// creating alert dialog class instance
		alert = new AlertDialogManager();

		mCancelButton = (Button) rootView.findViewById(R.id.cancelButton);
		mCancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().finish();
			}
		});

		mSubmitButton = (Button) rootView.findViewById(R.id.submitButton);
		mSubmitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// Get EditText values
				String p_name = mProductName.getText().toString();
				String p_id = "1";
				String sub_p_id = "2";
				String p_description = mProductDescription.getText().toString();
				String p_price = mProductPrice.getText().toString();
				String p_make = mProductBrand.getText().toString();
				String p_model = mProductModel.getText().toString();
				String p_condition = mProductCondition.getText().toString();
				String p_location = mProductLocation.getText().toString();

				// Instantiate Http Request Param Object
				RequestParams params = new RequestParams();

				// get Internet status
				isInternetPresent = cd.isConnectingToInternet();

				// check for Internet status
				if (isInternetPresent) {
					// Internet Connection is Present
					// validate text fields
					if (!FormValidationManager.isNotNull(p_name)) {
						mProductName.setError("Invalid Name");
					} else if (!FormValidationManager.isNotNull(p_description)) {
						mProductDescription.setError("Invalid Description");
					} else if (!FormValidationManager.isNotNull(p_price)) {
						mProductPrice.setError("Invalid Price");
					} else if (!FormValidationManager.isNotNull(p_make)) {
						mProductBrand.setError("Invalid Brand");
					} else if (!FormValidationManager.isNotNull(p_model)) {
						mProductModel.setError("Invalid Model");
					} else if (!FormValidationManager.isNotNull(p_condition)) {
						mProductCondition.setError("Invalid Condition");
					} else if (!FormValidationManager.isNotNull(p_location)) {
						mProductLocation.setError("Invalid Location");

					} else {
						params.put("p_catid", p_id);
						params.put("p_subcatid", sub_p_id);
						// Put Http parameter product name with value of Product
						// Name Edit View
						// control
						params.put("p_name", mProductName);
						// Put Http parameter product description with value of
						// Product Description Edit View
						// control
						params.put("p_description", mProductDescription);
						// Put Http parameter product price with value of
						// Product price Edit View
						// control
						params.put("p_price", mProductPrice);
						// Put Http parameter product brand with value of
						// Product Brand Edit View
						// control
						params.put("p_make", mProductBrand);
						// Put Http parameter product brand with value of
						// Product Model Edit View
						// control
						params.put("p_model", mProductModel);
						// Put Http parameter product brand with value of
						// Product Condition Edit View
						// control
						params.put("p_condition", mProductCondition);
						// Put Http parameter product brand with value of
						// promoter Location Edit View
						// control
						params.put("p_location", mProductLocation);
						// Invoke RESTful Web Service with Http parameters
						invokeWS(params);
					}
				} else {
					// Internet connection is not present
					// Ask user to connect to Internet
					alert.showAlertDialog(getActivity(),
							"No Internet Connection",
							"You don't have internet connection.", false);
				}
			}
		});
		return rootView;
	}

	/**
	 * Method that performs RESTful web service invocations
	 * 
	 * @param params
	 */
	public void invokeWS(RequestParams params) {

		// Instantiate Progress Dialog object
		pDialog = new ProgressDialog(getActivity());
		// Set Progress Dialog Text
		pDialog.setMessage("Please wait...");
		// Set Cancelable as False
		pDialog.setCancelable(false);
		// Show Progress Dialog
		pDialog.show();

		// Make RESTful webservice call using AsyncHttpClient object
		AsyncHttpClient client = new AsyncHttpClient();

		client.post(AD_SUBMIT_URL, params, new AsyncHttpResponseHandler() {
			// When the response returned by REST has Http response code '200'
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// TODO Auto-generated method stub
				// Hide Progress Dialog
				pDialog.hide();
				// When Http response code is '404'
				if (statusCode == 404) {
					Toast.makeText(getActivity().getApplicationContext(),
							"Requested resource not found", Toast.LENGTH_LONG)
							.show();
				}
				// When Http response code is '500'
				else if (statusCode == 500) {
					Toast.makeText(getActivity().getApplicationContext(),
							"Something went wrong at server end",
							Toast.LENGTH_LONG).show();
				}
				// When Http response code other than 404, 500
				else {
					Toast.makeText(
							getActivity().getApplicationContext(),
							"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]",
							Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] responseStream) {
				// TODO Auto-generated method stub
				// Hide Progress Dialog
				pDialog.hide();
				pDialog.dismiss();

				// Parse the response
				try {
					String response = IOUtils.toString(responseStream, "UTF_8");

					// JSON Object
					JSONObject obj = new JSONObject(response);

					// When the JSON response has status boolean value assigned
					// with true
					if ((obj.getBoolean("error")) != true) {

						// Display successfully message using Toast
						Toast.makeText(getActivity().getApplicationContext(),
								"Product added successfully!",
								Toast.LENGTH_LONG).show();
					}
					// Else display error message
					else {
						Toast.makeText(getActivity().getApplicationContext(),
								obj.getString("message"), Toast.LENGTH_LONG)
								.show();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				catch (JSONException e) {
					// TODO Auto-generated catch block
					Toast.makeText(
							getActivity().getApplicationContext(),
							"Error Occured [Server's JSON response might be invalid]!",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();

				}
			}
		});
	}

}
