package com.ulab.cwc;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ulab.cwc.utility.AlertDialogManager;
import com.ulab.cwc.utility.ConnectionDetector;
import com.ulab.cwc.utility.FormValidationManager;
import com.ulab.cwc.utility.SessionManager;

public class SignUpActivity extends Activity {

	private static final String REGISTER_URL = "http://192.168.56.1/code-warriors-challenge-15/Server-Side/v1/register";

	// progress dialog
	private ProgressDialog pDialog;

	// Session Manager Class
	private SessionManager session;

	// flag for Internet connection status
	private Boolean isInternetPresent = false;

	// Connection detector class
	private ConnectionDetector cd;

	// Alert Dialog Manager
	private AlertDialogManager alert;

	protected EditText mUsername;
	protected EditText fName;
	protected EditText lName;
	protected EditText mpassword;
	protected EditText mEmail;
	protected EditText mMobile;
	protected EditText mLocation;
	protected Button mSignUpButton;
	protected Button mCancelButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);

		mUsername = (EditText) findViewById(R.id.uNameField);
		fName = (EditText) findViewById(R.id.fNameField);
		lName = (EditText) findViewById(R.id.lNameField);
		mpassword = (EditText) findViewById(R.id.passwordField);
		mEmail = (EditText) findViewById(R.id.emailField);
		mMobile = (EditText) findViewById(R.id.mobileField);
		fName = (EditText) findViewById(R.id.fNameField);
		mLocation = (EditText) findViewById(R.id.locationField);

		// Session Manager
		session = new SessionManager(getApplicationContext());

		// creating connection detector class instance
		cd = new ConnectionDetector(getApplicationContext());

		// creating alert dialog class instance
		alert = new AlertDialogManager();

		mCancelButton = (Button) findViewById(R.id.cancelButton);
		mCancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		mSignUpButton = (Button) findViewById(R.id.signupButton);
		mSignUpButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// Get EditText values
				String username = mUsername.getText().toString();
				String first_name = fName.getText().toString();
				String last_name = lName.getText().toString();
				String email = mEmail.getText().toString();
				String password = mpassword.getText().toString();
				String mobile = mMobile.getText().toString();
				String location = mLocation.getText().toString();

				// Instantiate Http Request Param Object
				RequestParams params = new RequestParams();

				// get Internet status
				isInternetPresent = cd.isConnectingToInternet();

				// check for Internet status
				if (isInternetPresent) {
					// Internet Connection is Present
					// validate text fields
					if (!FormValidationManager.isNotNull(username)) {
						mUsername.setError("Invalid Username");
					} else if (!FormValidationManager.isNotNull(first_name)) {
						fName.setError("Invalid Name");
					} else if (!FormValidationManager.isNotNull(last_name)) {
						lName.setError("Invalid Name");
					} else if (!FormValidationManager.validate(email)) {
						mEmail.setError("Invalid Email");
					} else if (!FormValidationManager.isNotNull(password)) {
						mpassword.setError("Invalid Password");
					} else if (!FormValidationManager.isNotNull(mobile)) {
						mMobile.setError("Invalid number");
					}else if (!FormValidationManager.isNotNull(location)) {
						mLocation.setError("Invalid location");
					} else {
						// Put Http parameter username with value of Name Edit
						// View
						// control
						params.put("u_username", username);
						// Put Http parameter first name with value of Name Edit
						// View
						// control
						params.put("u_first_name", first_name);
						// Put Http parameter last name with value of Name Edit
						// View
						// control
						params.put("u_last_name", last_name);
						// Put Http parameter email with value of Email Edit
						// View
						// control
						params.put("u_email", email);
						// Put Http parameter password with value of Password
						// Edit View
						// control
						params.put("u_password", password);
						// Put Http parameter mobile number with value of
						// Password
						// Edit View
						// control
						params.put("u_mobile", mobile);
						// Put Http parameter location with value of
						// Password
						// Edit View
						// control
						params.put("u_location", location);
						// Put Http parameter type with value of
						// Password
						// Edit View
						// control
						params.put("u_type", "Personal");
						// Invoke RESTful Web Service with Http parameters
						invokeWS(params);
					}
				} else {
					// Internet connection is not present
					// Ask user to connect to Internet
					alert.showAlertDialog(SignUpActivity.this,
							"No Internet Connection",
							"You don't have internet connection.", false);
				}
			}
		});

	}

	/**
	 * Method that performs RESTful web service invocations
	 * 
	 * @param params
	 */
	public void invokeWS(RequestParams params) {

		// Instantiate Progress Dialog object
		pDialog = new ProgressDialog(this);
		// Set Progress Dialog Text
		pDialog.setMessage("Please wait...");
		// Set Cancelable as False
		pDialog.setCancelable(false);
		// Show Progress Dialog
		pDialog.show();

		// Make RESTful webservice call using AsyncHttpClient object
		AsyncHttpClient client = new AsyncHttpClient();

		client.post(REGISTER_URL, params, new AsyncHttpResponseHandler() {
			// When the response returned by REST has Http response code '200'
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// TODO Auto-generated method stub
				// Hide Progress Dialog
				pDialog.hide();
				// When Http response code is '404'
				if (statusCode == 404) {
					Toast.makeText(getApplicationContext(),
							"Requested resource not found", Toast.LENGTH_LONG)
							.show();
				}
				// When Http response code is '500'
				else if (statusCode == 500) {
					Toast.makeText(getApplicationContext(),
							"Something went wrong at server end",
							Toast.LENGTH_LONG).show();
				}
				// When Http response code other than 404, 500
				else {
					Toast.makeText(
							getApplicationContext(),
							"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]",
							Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] responseStream) {
				// TODO Auto-generated method stub
				// Hide Progress Dialog
				pDialog.hide();
				pDialog.dismiss();

				// Parse the response
				try {
					String response = IOUtils.toString(responseStream, "UTF_8");

					// JSON Object
					JSONObject obj = new JSONObject(response);

					// When the JSON response has status boolean value assigned
					// with true
					if ((obj.getBoolean("error")) != true) {

						// Creating user login session
						session.createLoginSession(mEmail.getText().toString(),
								mpassword.getText().toString());

						// Display successfully registered message using Toast
						Toast.makeText(getApplicationContext(),
								"You are successfully registered!",
								Toast.LENGTH_LONG).show();

						// Redirecting to the main class
						Intent i = new Intent(getApplicationContext(),
								MainActivity.class);
						i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(i);
						finish();
					}
					// Else display error message
					else {
						Toast.makeText(getApplicationContext(),
								obj.getString("message"), Toast.LENGTH_LONG)
								.show();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				catch (JSONException e) {
					// TODO Auto-generated catch block
					Toast.makeText(
							getApplicationContext(),
							"Error Occured [Server's JSON response might be invalid]!",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();

				}
			}
		});
	}

}
